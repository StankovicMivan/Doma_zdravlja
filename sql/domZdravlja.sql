DROP SCHEMA IF EXISTS domZdravlja;
CREATE SCHEMA domZdravlja DEFAULT CHARACTER SET utf8;
USE domZdravlja;

-- KREIRANJE TABELA
CREATE TABLE pacijent (
	id INT AUTO_INCREMENT, -- AUTO_INCREMENT - SUBP ce automatski odrediti vrednost ovog polja, ne treba navoditi vrednost pri ubacivanju novog sloga
	lbo BIGINT(11) UNIQUE,
	ime VARCHAR(20) NOT NULL,
	prezime VARCHAR(20)NOT NULL,
	datumRodjenja DATE NOT NULL,
	PRIMARY KEY(id) -- broj indeksa bi mogao biti primarni kljuc, ali je praksa da se uvede surogatni kljuc (to je ovo auto_increment polje student_id) o cijoj vrednosti baza vodi racuna i garantuje da je jedinstveno za svaki slog
);

CREATE TABLE tipPregleda (
	id INT AUTO_INCREMENT, -- AUTO_INCREMENT - SUBP ce automatski odrediti vrednost ovog polja, ne treba navoditi vrednost pri ubacivanju novog sloga
	nazivTipa VARCHAR(20) NOT NULL,
	cena DOUBLE(10,2) NOT NULL,
	PRIMARY KEY(id) -- broj indeksa bi mogao biti primarni kljuc, ali je praksa da se uvede surogatni kljuc (to je ovo auto_increment polje student_id) o cijoj vrednosti baza vodi racuna i garantuje da je jedinstveno za svaki slog
);
CREATE TABLE pregled (

	id INT AUTO_INCREMENT,
	datumIVreme DATETIME NOT NULL,
	tip INT ,
	pacijent_id INT,
	placen BOOLEAN NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (tip) REFERENCES tipPregleda(id),
	FOREIGN KEY (pacijent_id) REFERENCES pacijent(id)

);

INSERT INTO pacijent (id, lbo,ime,prezime,datumRodjenja) VALUES (1,00000000000,'Ivan','Stankovic','1995-03-29' );
INSERT INTO pacijent (id, lbo,ime,prezime,datumRodjenja) VALUES (2,00000000001,'Helena','Hip','1998-11-02' );
INSERT INTO pacijent (id, lbo,ime,prezime,datumRodjenja) VALUES (3,00000000002,'Jovana','Stankovic','1989-10-09' );
INSERT INTO pacijent (id, lbo,ime,prezime,datumRodjenja) VALUES (4,00000000003,'Miodrag','Stankovic','1964-05-09' );

INSERT INTO tipPregleda (id, nazivTipa,cena) VALUES (1,'opsti',2000);
INSERT INTO tipPregleda (id,nazivTipa, cena) VALUES (2,'specijalisticki', 3000);
INSERT INTO tipPregleda (id,nazivTipa, cena) VALUES (3,'dijagnosticki', 10000);
INSERT INTO tipPregleda (id,nazivTipa, cena) VALUES (4,'laboratorijski', 500);

INSERT INTO pregled (id,datumIVreme,tip,pacijent_id,placen) VALUES (1,'2018-05-14 12:00:00',1,1,'false');
INSERT INTO pregled (id,datumIVreme,tip,pacijent_id,placen) VALUES (2,'2018-05-14 12:20:00',1,2,'false');
INSERT INTO pregled (id,datumIVreme,tip,pacijent_id,placen) VALUES (3,'2018-05-14 12:40:00',1,3,'false');

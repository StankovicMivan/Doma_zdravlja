package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.HashMap;

import model.Pacijent;
import model.Pregled;
import model.TipPregleda;

public class PacijentDAO {

	public static Pacijent getPacijentiByLbo(Connection conn, String lbo) {
		Pacijent pacijent = null;
		Statement stmt = null;
		ResultSet rset = null;
		long lbo1 = 0;
		try {
			lbo1 = Long.parseLong(lbo);
		}catch(Exception e) {
			e.getMessage();
		}
		try {
			String query = "SELECT * FROM pacijent WHERE pacijent.lbo = '" + lbo1 + "'";
			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);
			if(rset.next()) {
				int id = rset.getInt(1);
				String ime = rset.getString(3);
				String prezime = rset.getString(4);
				Date datum = rset.getDate(5);
				
				pacijent = new Pacijent(id, lbo1, ime, prezime, datum);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		return pacijent;
	}

	public static boolean add(Connection conn, Pacijent pacijent) {
		PreparedStatement pstmt = null;
		try {
			String query = "INSERT INTO pacijent (lbo, ime, prezime, datumRodjenja) VALUES (?, ?, ?, ?)";
			pstmt = conn.prepareStatement(query);
			pstmt.setLong(1, pacijent.getLbo());
			pstmt.setString(2, pacijent.getIme());
			pstmt.setString(3, pacijent.getPrezime());
			pstmt.setDate(4, pacijent.getDatumRodjenja());
			pstmt.executeUpdate();
			return true;
		}catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		return false;
	}
	public static Pacijent GetPacijentById(Connection conn, int id) {
		Pacijent pacijent = null;
		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT * FROM pacijent WHERE id = " + id;
			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);
			if(rset.next()) {
				int index =2;
				long lbo = rset.getLong(index++);
				String ime = rset.getString(index++);
				String prezime = rset.getString(index++);
				Date datum = rset.getDate(index++);
				
				pacijent = new Pacijent(id, lbo, ime, prezime, datum);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		return pacijent;
	}
	public static HashMap<Integer, Pacijent> getAll(Connection conn) {
		HashMap<Integer, Pacijent> mapaPacijenata = new HashMap<>();
		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT * FROM pacijent JOIN pregled ON pregled.pacijent_id = pacijent.id";

			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);

			while (rset.next()) {
				int id = rset.getInt(1);
				if(mapaPacijenata.get(id) == null) {
					long lbo = rset.getLong(2);
					String ime = rset.getString(3);
					String prezime = rset.getString(4);
					Date datumRodj = rset.getDate(5);
					Pacijent pacijent = new Pacijent(id, lbo, ime, prezime, datumRodj);
					mapaPacijenata.put(id, pacijent);
				}
				int idPregleda = rset.getInt(6);
				Timestamp datumVreme = rset.getTimestamp(7);
				int idTipa = rset.getInt(8);
				TipPregleda tip = TipPregledaDAO.getTipByID(conn, idTipa);
				boolean placen = rset.getBoolean(10);
				Pregled pregled = new Pregled(idPregleda, datumVreme,tip, mapaPacijenata.get(id), placen);
				mapaPacijenata.get(id).getPregledi().add(pregled);
				
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mapaPacijenata;
	}
}

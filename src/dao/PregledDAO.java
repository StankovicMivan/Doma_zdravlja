package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import model.Pacijent;
import model.Pregled;

import model.TipPregleda;

public class PregledDAO {

	public static boolean add(Connection conn, Pregled newPregled) {
		PreparedStatement pstmt = null;
		try {
			String query = "INSERT INTO pregled (datumIVreme, tip, pacijent_id, placen) VALUES (?, ?, ?, ?)";
			pstmt = conn.prepareStatement(query);
			int index =1;
			
			pstmt.setTimestamp(index++, newPregled.getDatumIVreme());
			pstmt.setInt(index++, newPregled.getTip().getId());
			pstmt.setInt(index++, newPregled.getPacijent().getId());
			pstmt.setBoolean(index++, newPregled.isPlatio());
			
			pstmt.executeUpdate();
			return true;
		}catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		return false;
	}

	public static List<Pregled> getNeplaceniByID(Connection conn, int id) {
		List<Pregled> neplaceni = new ArrayList<>();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT * FROM pregled JOIN pacijent ON pregled.id = pacijent.id WHERE pacijent.id = ? AND pregled.placen = ?";

			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, id);
			pstmt.setBoolean(2, false);
			rset = pstmt.executeQuery();

			while (rset.next()) {
				int index = 1;
				int idPregleda = rset.getInt(index++);
				Timestamp datum = rset.getTimestamp(index++);
				int tipId = rset.getInt(index++);
				TipPregleda tip = TipPregledaDAO.getTipByID(conn, tipId);
				int pacijentId = rset.getInt(index++);
				Pacijent pacijent = PacijentDAO.GetPacijentById(conn, pacijentId);
				Pregled pregled = new Pregled(idPregleda, datum, tip, pacijent, false);
				neplaceni.add(pregled);
				
			}
			
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return neplaceni;
	}

	public static boolean update(Connection conn, int id) {
		PreparedStatement pstmt = null;
		try {
			String query = "UPDATE pregled SET placen = ? WHERE id = ?";
			pstmt = conn.prepareStatement(query);
			pstmt.setBoolean(1, true);
			pstmt.setInt(2, id);
			pstmt.executeUpdate();
			return true;
		}catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		return false;
	}

}

package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import model.TipPregleda;



public class TipPregledaDAO {

	public static TipPregleda getTipByID(Connection conn, int id) {
		TipPregleda tip = null;
		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT * FROM tippregleda WHERE id = " + id;
			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);
			if(rset.next()) {
				String naziv = rset.getString(2);
				double cena = rset.getDouble(3);
				tip = new TipPregleda(id, naziv, cena);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		return tip;
	}
}

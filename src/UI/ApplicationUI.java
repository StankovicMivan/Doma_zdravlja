package UI;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.jakewharton.fliptables.FlipTable;

import Util.PomocnaKlasa;
import dao.PacijentDAO;

import dao.PregledDAO;

import dao.TipPregledaDAO;
import model.Pacijent;
import model.Pregled;
import model.TipPregleda;





public class ApplicationUI {

	public static void ispisiTekstOsnovneOpcije(){	
		System.out.println("Dom zdravlja - Osnovne opcije:");
		System.out.println("\tOpcija broj 1 - Dodavanje pacijenta");
		System.out.println("\tOpcija broj 2 - Dodavanje pregleda");
		System.out.println("\tOpcija broj 3 - Prikaz svih pacijenata");
		System.out.println("\tOpcija broj 4 - Ulata neplacenih");
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ IZ PROGRAMA");	
	}
	public static void main(String[] args) {


		Connection conn;
		try {

			Class.forName("com.mysql.jdbc.Driver");

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/domzdravlja?useSSL=false", "root","root");
		} catch (Exception ex) {
			System.out.println("Neuspela konekcija na bazu!");
			ex.printStackTrace();

			return;
		}
		//ocitava bazu i smesta je u liste koje se nalaze u PopuniListe klasi

		int odluka = -1;
		while(odluka!= 0){
			ApplicationUI.ispisiTekstOsnovneOpcije();
			System.out.print("opcija:");
			odluka = PomocnaKlasa.ocitajCeoBroj();
			switch (odluka) {
			case 0:	
				System.out.println("Izlaz iz programa");	
				break;
			case 1:
				dodajPacijenta(conn);
				break;
			case 2:
				dodajPregled(conn);
				break;
			case 3:
				prikazSvihPacijenata(conn);
				break;
			case 4:
				uplata(conn);
				break;
			default:
				System.out.println("Nepostojeca komanda");
				break;
			}
		}

		//zatvaranje konekcije
		try {
			conn.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
	private static void uplata(Connection conn) {
		System.out.println("Unesite id pacijenta:");
		int id = PomocnaKlasa.ocitajCeoBroj();
		while(PacijentDAO.GetPacijentById(conn, id) == null) {
			System.out.println("Unesite id pacijenta:");
			id = PomocnaKlasa.ocitajCeoBroj();
		}
	
		List<Pregled> listaNeplacenih = PregledDAO.getNeplaceniByID(conn, id);
		if(listaNeplacenih.isEmpty()) {
			System.out.println("Nema neplacenih pregleda");
			return;
		}
		System.out.println("Neplaceni pregledi");
		for(int i = 0; i < listaNeplacenih.size(); i++) {
			System.out.println(listaNeplacenih.get(i));
		}
		if(PomocnaKlasa.ocitajOdlukuOPotvrdi(" da platite sve racune?") == 'Y') {
			for(int i = 0; i < listaNeplacenih.size(); i++) {
				if(!PregledDAO.update(conn,listaNeplacenih.get(i).getId())) {
					System.out.println("Ne realizovana izmena kod pregleda sa id " + listaNeplacenih.get(i).getId());
				}
			}
		}
	}
	private static void prikazSvihPacijenata(Connection conn) {

		HashMap<Integer, Pacijent> listaSvih = PacijentDAO.getAll(conn);
		for(Pacijent pacijent: listaSvih.values()) {
			System.out.println(pacijent);
			ArrayList<Pregled> pregledi = pacijent.getPregledi();
			String[] header =  {"Datum i vreme", "Naziv tipa", "Cena", "Placen"};
			String[][] data = new String[pregledi.size()][4];
			for(int i = 0; i < pregledi.size(); i++) {
				data[i][0] = String.valueOf(pregledi.get(i).getDatumIVreme());
				data[i][1] = pregledi.get(i).getTip().getNazivTipa();
				data[i][2] = String.valueOf(pregledi.get(i).getTip().getCena());
				data[i][3] = String.valueOf(pregledi.get(i).isPlatio());

			}
			System.out.println(FlipTable.of(header, data));
		}


	}
	private static void dodajPregled(Connection conn) {
		System.out.println("Unesite id pacijenta: ");
		int id_pacijenta = PomocnaKlasa.ocitajCeoBroj();
		Pacijent pacijent = PacijentDAO.GetPacijentById(conn, id_pacijenta);

		System.out.println("Unesite id tipa pregleda: ");
		int id_tipa = PomocnaKlasa.ocitajCeoBroj();
		TipPregleda tip = TipPregledaDAO.getTipByID(conn, id_tipa);

		Timestamp time = new Timestamp(System.currentTimeMillis());
		boolean platio = false;
		Pregled newPregled = new Pregled(0,time,tip,pacijent,platio);
		if(PregledDAO.add(conn,newPregled)) {
			System.out.println("Pregled je uspesno dodat u bazu");
		}
	}
	private static void dodajPacijenta(Connection conn) {
		System.out.println("Unesite LBO (11 brojeva)");
		String temp = PomocnaKlasa.ocitajTekst();
		while(PacijentDAO.getPacijentiByLbo(conn, temp)!= null || !PomocnaKlasa.is11(temp)) {
			System.out.println("Uneti lbo vec postoji unesite ponovo, Unesite LBO");
			temp = PomocnaKlasa.ocitajTekst();
		}
		long lbo;
		lbo = Long.parseLong(temp);	

		System.out.println("Unesite ime pacijenta:");
		String ime = PomocnaKlasa.ocitajTekst();

		System.out.println("Unesite prezime pacijenta:");
		String prezime = PomocnaKlasa.ocitajTekst();

		System.out.println("Unesite datum rodjenja u formatu dd.MM.yyyy. :");
		Date datum = PomocnaKlasa.ocitajDatum();
		Pacijent pacijent = new Pacijent(0, lbo, ime, prezime, datum);
		if(PacijentDAO.add(conn, pacijent)) {
			System.out.println("Uspesno dodato!");
		}

	}
}

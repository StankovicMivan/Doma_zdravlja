package model;

import java.sql.Timestamp;

public class Pregled {

	private int id;
	private Timestamp datumIVreme;
	private TipPregleda tip;
	private Pacijent pacijent;
	private boolean platio;
	public Pregled() {
		super();
	}
	public Pregled(int id, Timestamp datumIVreme, TipPregleda tip, Pacijent pacijent, boolean platio) {
		super();
		this.id = id;
		this.datumIVreme = datumIVreme;
		this.tip = tip;
		this.pacijent = pacijent;
		this.platio = platio;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Timestamp getDatumIVreme() {
		return datumIVreme;
	}
	public void setDatumIVreme(Timestamp datumIVreme) {
		this.datumIVreme = datumIVreme;
	}
	public TipPregleda getTip() {
		return tip;
	}
	public void setTip(TipPregleda tip) {
		this.tip = tip;
	}
	public Pacijent getPacijent() {
		return pacijent;
	}
	public void setPacijent(Pacijent pacijent) {
		this.pacijent = pacijent;
	}
	public boolean isPlatio() {
		return platio;
	}
	public void setPlatio(boolean platio) {
		this.platio = platio;
	}
	@Override
	public String toString() {
		return id + ", datum kreiranja " + datumIVreme + ", pacijent " + pacijent.getIme() + " "+ pacijent.getPrezime() + ", tip pregleda " + tip.getNazivTipa()
				+ ", placen " + platio;
	}
	
	
	
}

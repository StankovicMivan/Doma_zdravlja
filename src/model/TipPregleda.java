package model;


public class TipPregleda {

	private int id;
	private String NazivTipa;
	private double cena;
	public TipPregleda() {
		super();
	}
	public TipPregleda(int id, String nazivTipa, double cena) {
		super();
		this.id = id;
		NazivTipa = nazivTipa;
		this.cena = cena;
	}
	@Override
	public String toString() {
		return "tipPregleda [id=" + id + ", NazivTipa=" + NazivTipa + ", cena=" + cena + "]";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNazivTipa() {
		return NazivTipa;
	}
	public void setNazivTipa(String nazivTipa) {
		NazivTipa = nazivTipa;
	}
	public double getCena() {
		return cena;
	}
	public void setCena(double cena) {
		this.cena = cena;
	}
	
	
	
}

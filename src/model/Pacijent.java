package model;

import java.sql.Date;
import java.util.ArrayList;

public class Pacijent {

	private int id;
	private long lbo;
	private String ime;
	private String prezime;
	private Date datumRodjenja;
	private ArrayList<Pregled> pregledi = new ArrayList<>();
	public Pacijent() {
		super();
	}
	
	public Pacijent(int id, long lbo, String ime, String prezime, Date datumRodjenja, ArrayList<Pregled> pregledi) {
		super();
		this.id = id;
		this.lbo = lbo;
		this.ime = ime;
		this.prezime = prezime;
		this.datumRodjenja = datumRodjenja;
		this.pregledi = pregledi;
	}


	public Pacijent(int id, long lbo, String ime, String prezime, Date datumRodjenja) {
		super();
		this.id = id;
		this.lbo = lbo;
		this.ime = ime;
		this.prezime = prezime;
		this.datumRodjenja = datumRodjenja;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public long getLbo() {
		return lbo;
	}
	public void setLbo(long lbo) {
		this.lbo = lbo;
	}
	public ArrayList<Pregled> getPregledi() {
		return pregledi;
	}
	
	public void setPregledi(ArrayList<Pregled> pregledi) {
		this.pregledi = pregledi;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public Date getDatumRodjenja() {
		return datumRodjenja;
	}
	public void setDatumRodjenja(Date datumRodjenja) {
		this.datumRodjenja = datumRodjenja;
	}
	@Override
	public String toString() {
		return id + ". Lbo: " + lbo + ", ime i prezime pacijenta " + ime + " " + prezime + ", datum rodjenja "
				+ datumRodjenja;
	}
	
	
}
